<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Brands
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Brands </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Brands</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Brands 
            <a class="btn btn-sm btn-primary" href="{{ route('brands.port') }}">Excel Export</a>
            <a class="btn btn-sm btn-primary" href="{{ route('brands.pdf') }}">PDF Download</a>
            <a class="btn btn-sm btn-danger" href="{{ route('brands.trashed') }}">Trashed List</a>
             <a class="btn btn-sm btn-info" href="{{ route('brands.create') }}">Add New</a>
        </div>
        <div class="card-body">

           <x-backend.layouts.elements.message :message="session('message')" />

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>Sl#</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $sl=0 @endphp
                    @foreach ($brands as $brand)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $brand->title }}</td>
                        <td>{{ $brand->description }}</td>
                        <td>
                            <img src="{{ asset('storage/images/'.$brand->image) }}" alt="{{ $brand->title }}"
                            width="100" height="100">
                        </td>
                        <td>
                            <a class="btn btn-info btn-sm" href="{{ route('brands.show', ['brand' => $brand->id]) }}" >Show</a>

                            <a class="btn btn-warning btn-sm" href="{{ route('brands.edit', ['brand' => $brand->id]) }}" >Edit</a>

                            <form style="display:inline" action="{{ route('brands.destroy', ['brand' => $brand->id]) }}" method="post">
                                @csrf
                                @method('delete')
                                
                                <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                            </form>

                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</x-backend.layouts.master>