<?php

namespace App\Http\Controllers;

use App\Exports\CategoryExport;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Image;
use Excel;
use PDF;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::latest()->get();
        return view('backend.categories.index', [
            'categories' => $categories
        ]);
    }

    public function create()
    {
        return view('backend.categories.create');
    }

    public function store(CategoryRequest $request)
    {
        try {           
            Category::create([
                'title' => $request->title,
                'description' => $request->description,
                'image' => $this->uploadImage(request()->file('image'))
            ]);

            return redirect()->route('categories.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Category $category)
    {
        return view('backend.categories.show', [
            'category' => $category
        ]);
    }

    public function edit(Category $category)
    {
        return view('backend.categories.edit', [
            'category' => $category
        ]);
    }

    public function update(CategoryRequest $request, Category $category)
    {
        try {
            $requestData = [
                'title' => $request->title,
                'description' => $request->description,
            ];

            if($request->hasFile('image')){
                $requestData['image'] = $this->uploadImage(request()->file('image'));
            }

            $category->update($requestData);

            return redirect()->route('categories.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Category $category)
    {
        try {
            $category->delete();
            return redirect()->route('categories.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    // Softdelete
    public function trash()
    {
        $categories = Category::onlyTrashed()->get();

        return view('backend.categories.trashed', [
            'categories' => $categories
        ]);        
    }

    public function restore($id)
    {
        $category = Category::onlyTrashed()->findOrFail($id);
        $category->restore();
        return redirect()->route('categories.trashed')->withMessage('Successfully Restored!');
    }

    public function delete($id)
    {
        $category = Category::onlyTrashed()->findOrFail($id);
        $category->forceDelete();
        return redirect()->route('categories.trashed')->withMessage('Successfully Deleted Permanently!');
    }

    public function uploadImage($file)
    {        
        $fileName = time().'.'.$file->getClientOriginalExtension();
        Image::make($file)
                ->resize(200, 200)
                ->save(storage_path().'/app/public/images/'.$fileName);
        return $fileName;
    }

    public function export() 
    {
        return Excel::download(new CategoryExport, 'categories.xlsx');
    }

    public function downloadPdf()
    {
        $categories = Category::all();
        $pdf = PDF::loadView('backend.categories.pdf', compact('categories'));
        return $pdf->download('categories.pdf');
    }

}
