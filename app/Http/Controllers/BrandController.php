<?php

namespace App\Http\Controllers;

use App\Exports\BrandExport;
use App\Http\Requests\BrandRequest;
use App\Models\Brand;
use Illuminate\Database\QueryException;
use Image;
use PDF;
use Excel;
class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::latest()->get();
        return view('backend.brands.index', [
            'brands' => $brands
        ]);
    }
    public function create()
    {
        return view('backend.brands.create');
    }

    public function store(BrandRequest $request)
    {
        try {           
            Brand::create([
                'title' => $request->title,
                'description' => $request->description,
                'image' => $this->uploadImage(request()->file('image'))
            ]);

            return redirect()->route('brands.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Brand $brand)
    {
        return view('backend.brands.show', [
            'brand' => $brand
        ]);
    }

    public function edit(Brand $brand)
    {
        return view('backend.brands.edit', [
            'brand' => $brand
        ]);
    }

    public function update(BrandRequest $request, Brand $brand)
    {
        try {
            $requestData = [
                'title' => $request->title,
                'description' => $request->description,
            ];

            if($request->hasFile('image')){
                $requestData['image'] = $this->uploadImage(request()->file('image'));
            }

            $brand->update($requestData);

            return redirect()->route('brands.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Brand $brand)
    {
        try {
            $brand->delete();
            return redirect()->route('brands.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    // Softdelete
    public function trash()
    {
        $brands = Brand::onlyTrashed()->get();

        return view('backend.brands.trashed', [
            'brands' => $brands
        ]);        
    }

    public function restore($id)
    {
        $brand = Brand::onlyTrashed()->findOrFail($id);
        $brand->restore();
        return redirect()->route('brands.trashed')->withMessage('Successfully Restored!');
    }

    public function delete($id)
    {
        $brand = Brand::onlyTrashed()->findOrFail($id);
        $brand->forceDelete();
        return redirect()->route('brands.trashed')->withMessage('Successfully Deleted Permanently!');
    }

    public function uploadImage($file)
    {        
        $fileName = time().'.'.$file->getClientOriginalExtension();
        Image::make($file)
                ->resize(200, 200)
                ->save(storage_path().'/app/public/images/'.$fileName);
        return $fileName;
    }

    public function downloadPdf()
    {
        $brands = Brand::all();
        $pdf = PDF::loadView('backend.brands.pdf', compact('brands'));
        return $pdf->download('brands.pdf');
    }

    public function port() 
    { 
        return Excel::download(new BrandExport, 'brands.xlsx');
    }


}
